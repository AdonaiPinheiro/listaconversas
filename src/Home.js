import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import ListItem from './ListItem'

export default class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            list:[
                {key:'1', img:'https://www.b7web.com.br/avatar1.png', nome:'Adonai', msg:'Quando sai novas aulas?'},
                {key:'2', img:'https://www.b7web.com.br/avatar2.png', nome:'João', msg:'E ai seu arrombado'},
                {key:'3', img:'https://www.b7web.com.br/avatar3.png', nome:'Maria', msg:'Oi, gostosão!'},
                {key:'4', img:'https://www.b7web.com.br/avatar1.png', nome:'Pedro', msg:'Ow mano, e aquele projeto?'},
                {key:'5', img:'https://www.b7web.com.br/avatar2.png', nome:'José', msg:'150,00 reais'},
                {key:'6', img:'https://www.b7web.com.br/avatar3.png', nome:'Milena', msg:'Quero você arrombado!'},
            ],
        };
    }

    render(){
        return(

            <View style={styles.container}>
                <FlatList
                    data={this.state.list}
                    renderItem={(item) => <ListItem data={item} />}
                />
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container:{

    }
});