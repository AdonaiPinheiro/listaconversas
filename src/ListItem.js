import React, { Component } from 'react';
import { View, Text, Image, TouchableHighlight, StyleSheet } from 'react-native';

export default class ListItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            msg:props.data.item.msg
        }
        this.click = this.click.bind(this);
    }

    click(){
        alert(this.props.data.item.key);
    }

    render() {
        return(

            <TouchableHighlight onPress={this.click} underlayColor="#CCC" >

                <View style={styles.item}>
                    <Image source={{ uri:this.props.data.item.img }} style={styles.image} />
                    <View style={styles.msgContainer}>
                        <Text style={styles.msgTitulo}>{this.props.data.item.nome}</Text>
                        <Text numberOfLines={1} style={styles.msgConteudo}>{this.state.msg}</Text>
                    </View>
                </View>

            </TouchableHighlight>

        );
    }
}

const styles = StyleSheet.create({
    item:{
        height:60,
        marginLeft:10,
        marginRight:10,
        borderBottomWidth:1,
        borderColor:"#CCC",
        flex:1,
        flexDirection:'row'
    },
    image:{
        width:40,
        height:40,
        borderRadius:20,
        marginTop:10
    },
    msgContainer:{
        marginLeft:10,
        flex:1,
        justifyContent:'center'
    },
    msgTitulo:{
        fontWeight:'bold',
        fontSize:16
    }
});