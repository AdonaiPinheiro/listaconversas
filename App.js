import React, { Component } from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';

import Home from './src/Home'

const AppNavigation = createStackNavigator({
  Home:{
    screen:Home
  }
}, {
  defaultNavigationOptions:{
    header: null
  }
});

export default createAppContainer(AppNavigation);